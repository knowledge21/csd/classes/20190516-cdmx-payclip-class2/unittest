import org.junit.Assert;
import org.junit.Test;

public class TestCommisionCalculator {

    @Test
    public void Test5ksalesgivescomitionsof250(){
        double sale = 5000;
        double com = 250;
        double actualCommision = new CommisionCalculator().getCommision(sale);


        Assert.assertEquals(actualCommision, com,0);
    }
    @Test
    public void testNoSalesGiveComisionof0(){
        double sale =0;
        double comision =0;

        double actualCommision = new  CommisionCalculator().getCommision(sale);
        Assert.assertEquals(actualCommision, comision,0);
    }

    @Test
    public void test12kSaleGiveCommisionof720(){
        double sale =12000;
        double comision =720;

        double actualCommision = new  CommisionCalculator().getCommision(sale);
        Assert.assertEquals(actualCommision, comision,0);
    }

    @Test
    public void test1_5SaleGiveCommisionof(){
        double sale =1.5;
        double comision = 0.07;

        double actualCommision = new  CommisionCalculator().getCommision(sale);
        Assert.assertEquals(actualCommision, comision,0.001);
    }

    @Test
    public void test_Comission_Round(){
        double sale = 55.59;
        double comision = 2.77;

        double actualCommision = new  CommisionCalculator().getCommision(sale);
        Assert.assertEquals(actualCommision, comision,0.001);
    }

    @Test
    public void test_6percent_commi(){
        double sale = 14756.44;
        double comision = 885.38;

        double actualCommision = new  CommisionCalculator().getCommision(sale);
        Assert.assertEquals(actualCommision, comision,0.001);
    }
}



