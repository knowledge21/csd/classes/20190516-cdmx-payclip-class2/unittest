import sun.nio.cs.ext.MacArabic;

public class CommisionCalculator {

    public double getCommision(double sale) {
        double multiplicacion= 0;

        if (sale > 10000)
            multiplicacion = sale*.06;
        else
            multiplicacion = sale*.05;

        return Math.floor(multiplicacion*100)/100;
    }
}
